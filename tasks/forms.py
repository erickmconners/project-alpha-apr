from django.forms import ModelForm
from tasks.models import Task
from django.forms.widgets import DateTimeInput


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            "start_date": DateTimeInput(attrs={"type": "datetime-local"}),
            "due_date": DateTimeInput(attrs={"type": "datetime-local"}),
        }
